<?php

namespace tests;

use TripSorter\PassesStack;

include __DIR__ . DIRECTORY_SEPARATOR . '../src/autoload.php';
/**
 * Testing a journay from Madrid to NYC
 * Class MadridToNYCTest
 */

class MadridToNYCTest
{

    protected function loadData()
    {
        try {
            return file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'madrid_new_york.json');
        } catch (\Exception $e) {
            echo 'Test data file not found' . PHP_EOL;
            exit;
        }
    }

    /**
     * @throws \Exception
     * @return bool
     */
    public function testCreatingAStack()
    {
        $passesJSON = $this->loadData();
        $stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);

        if (!is_array($stack->getRawPasses())) {
            echo('Creating of stack failed' . PHP_EOL);
            return false;
        }

        if (sizeof($stack->getRawPasses()) == 0) {
            echo('Creating of stack failed: empty stack created' . PHP_EOL);
            return false;
        }
        echo 'Creating A Stack: OK' . PHP_EOL;
        return true;
    }

    public function testGetStackDescription()
    {
        $passesJSON = $this->loadData();

        $stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);
        $stack->sort();

        echo (strcmp($stack->getJourneyDescription() , '1. Take train 78A from Madrid to Barcelona. Sit in seat 45B
2. Take the airport bus from Barcelona to Gerona Airport. No seat assignment.
3. From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at
 ticket counter 344.
4. From Stockholm, take flight SK455 to New York JFK. Gate 22, seat 7B. Baggage will we au
tomatically transferred from your last leg.
5. You have arrived at your final destination.')) ? 'Trip description OK' : 'Trip description wrong';
        echo PHP_EOL;
    }

    /**
     * Compares ordered list in JSON format with expected
     * @throws \Exception
     */
    public function testSorting()
    {
        $passesJSON = $this->loadData();
        $stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);

        $stack->sort();

        echo (strcmp($stack->outputOrderedPasses(), '{"items":[{"type":"train","transportNumber":"78A","from":"Madrid","destination":"Barcelona
","seat":"45B","gate":"","baggage":""},{"type":"airport bus","transportNumber":"","from":"
Barcelona","destination":"Gerona Airport","seat":"","gate":"","baggage":""},{"type":"airpl
ane","transportNumber":"SK455","from":"Gerona Airport","destination":"Stockholm","seat":"3
A","gate":"45B","baggage":"Baggage drop at ticket counter 344."},{"type":"airplane","trans
portNumber":"SK455","from":"Stockholm","destination":"New York JFK","seat":"7B","gate":"22
","baggage":"Baggage will we automatically transferred from your last leg."}]}')) ? 'Ordered list is OK' : 'Ordered list is wrong';
    }

}

// Run tests
(new MadridToNYCTest())->testCreatingAStack();
(new MadridToNYCTest())->testGetStackDescription();
(new MadridToNYCTest())->testSorting();