<?php

namespace tests;

use TripSorter\PassesStack;

include __DIR__ . DIRECTORY_SEPARATOR . '../src/autoload.php';

/**
 * Testing a journey from NY to SF
 * Class OneStepTripTest
 */

class OneStepTripTest
{

    protected function loadData()
    {
        try {
            return file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'one_step_trip.json');
        } catch (\Exception $e) {
            echo 'Test data file not found' . PHP_EOL;
            exit;
        }
    }

    public function testGetStackDescription()
    {
        $passesJSON = $this->loadData();

        $stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);
        $stack->sort();

        echo (strcmp($stack->getJourneyDescription() , '1. From Newark Liberty, take flight UAL93 to San Francisco. Gate 45B, seat 3A. Baggage drop at
 ticket counter 344.
2. You have arrived at your final destination.')) ? 'Trip description OK' : 'Trip description wrong';
        echo PHP_EOL;
    }

    /**
     * Compares ordered list in JSON format with expected
     * @throws \Exception
     */
    public function testSorting()
    {
        $passesJSON = $this->loadData();

        $stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);

        $stack->sort();

        echo (strcmp($stack->outputOrderedPasses(), '{"items":[{"type":"airplane","transportNumber":"UAL93","from":"Newark Liberty","destinatio
n":"San Francisco","seat":"3A","gate":"45B","baggage":"Baggage drop at ticket counter 344.
"}]}')) ? 'Ordered list is OK' : 'Ordered list is wrong';
    }
}

// Run tests
(new OneStepTripTest())->testGetStackDescription();
(new OneStepTripTest())->testSorting();