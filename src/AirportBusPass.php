<?php

namespace TripSorter;

/**
 * Airport Bus boarding pass
 * Class TrainPass
 * @package TripSorter
 */
class AirportBusPass extends BoardingPass
{
    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        $description = 'Take the airport bus from '
            . $this->getFrom()
            . ' to ' . $this->getDestination() . '. No seat assignment.';
        return $description;
    }
}
