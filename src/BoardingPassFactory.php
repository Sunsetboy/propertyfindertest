<?php

namespace TripSorter;

/**
 * Factory class for creating instances of boarding passes
 * Class BoardingPassFactory
 * @package TripSorter
 */
class BoardingPassFactory
{
    /**
     * @param array $boardingPassSettings Array with pass settings
     * @return BoardingPass
     * @throws \Exception
     */
    public function create(array $boardingPassSettings)
    {
        switch ($boardingPassSettings['type']) {
            case 'airplane':
                return new AirplanePass($boardingPassSettings);
                break;
            case 'train':
                return new TrainPass($boardingPassSettings);
                break;
            case 'airport bus':
                return new AirportBusPass($boardingPassSettings);
                break;
            default:
                throw new \Exception('Invalid Transport type');
        }
    }
}
