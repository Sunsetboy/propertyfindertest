<?php

namespace TripSorter;

/**
 * The base class for all transportation types
 * Class BoardingPass
 * @package TripSorter
 */
abstract class BoardingPass
{
    const TYPE_AIRPLANE = 1;
    const TYPE_TRAIN = 2;
    const TYPE_BUS = 3;


    /**
     * BoardingPass constructor.
     * @param $settings
     */
    public function __construct($settings)
    {
        $this->setType($settings['type']);
        $this->setTransportNumber($settings['transportNumber']);
        $this->setFrom($settings['from']);
        $this->setDestination($settings['destination']);
        $this->setSeat($settings['seat']);
        $this->setGate($settings['gate']);
        $this->setBaggage($settings['baggage']);
    }

    /**
     * Type of transport
     * @var integer
     */
    protected $type;

    /**
     * Number of train, flight, etc
     * @var string
     */
    protected $transportNumber;

    /**
     * Start point name
     * @var string
     */
    protected $from;

    /**
     * Destination name
     * @var string
     */
    protected $destination;

    /**
     * Seat number
     * @var string
     */
    protected $seat;

    /**
     * Gate number
     * @var string
     */
    protected $gate;

    /**
     * Baggage information
     * @var string
     */
    protected $baggage;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }

    /**
     * @param string $transportNumber
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return string
     */
    public function getSeat()
    {
        return $this->seat;
    }

    /**
     * @param string $seat
     */
    public function setSeat($seat)
    {
        $this->seat = $seat;
    }

    /**
     * @return string
     */
    public function getGate()
    {
        return $this->gate;
    }

    /**
     * @param string $gate
     */
    public function setGate($gate)
    {
        $this->gate = $gate;
    }

    /**
     * @return string
     */
    public function getBaggage()
    {
        return $this->baggage;
    }

    /**
     * @param string $baggage
     */
    public function setBaggage($baggage)
    {
        $this->baggage = $baggage;
    }


    /**
     * Returns a description of journey part
     * @return string
     */
    abstract public function getDescription();

    /**
     * Returns an array of possible transport types
     * @return array Transport types names
     */
    public function getTypes()
    {
        return [
            self::TYPE_AIRPLANE => 'airplane',
            self::TYPE_BUS => 'bus',
            self::TYPE_TRAIN => 'train',
        ];
    }

}
