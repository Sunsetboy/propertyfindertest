<?php

namespace TripSorter;

/**
 * A stack of boarding passes
 * Class PassesStack
 * @package TripSorter
 */
class PassesStack
{

    const FORMAT_JSON = 'json';

    protected $format;
    /**
     * A description of the route
     * @var string
     */
    protected $description;

    /**
     * An unordered set of boarding passes
     * @var BoardingPass[]
     */
    protected $rawPasses = [];

    /**
     * An ordered set of boarding passes
     * @var BoardingPass[]
     */
    protected $orderedPasses = [];

    /**
     * @return BoardingPass[]
     */
    public function getRawPasses()
    {
        return $this->rawPasses;
    }

    /**
     * @param BoardingPass[] $rawPasses
     */
    public function setRawPasses($rawPasses)
    {
        $this->rawPasses = $rawPasses;
    }

    /**
     * @return BoardingPass[]
     */
    public function getOrderedPasses()
    {
        return $this->orderedPasses;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param BoardingPass[] $orderedPasses
     */
    public function setOrderedPasses($orderedPasses)
    {
        $this->orderedPasses = $orderedPasses;
    }

    /**
     * PassesStack constructor.
     * Creates an unordered set of passes
     * @param mixed $passes Boarding passes in some format
     * @param string $format input data format
     * @throws \Exception
     */
    public function __construct($passes, $format)
    {
        switch ($format) {
            case self::FORMAT_JSON:
                $passesArray = json_decode($passes, true);
                $this->setDescription($passesArray['description']);

                $passesEntities = [];
                $boardingPassFactory = new BoardingPassFactory();

                foreach ($passesArray['items'] as $item) {
                    $passesEntities[] = $boardingPassFactory->create($item);
                }
                $this->setRawPasses($passesEntities);
                break;
            default:
                throw new \Exception('Invalid format of input data');
        }

        $this->setFormat($format);
    }

    /**
     * Orders boarding passes by destinations
     */
    public function sort()
    {
        $rawPassesTempArray = $this->rawPasses;

        // Move first element from the unordered list to the ordered list
        $initialElement = array_shift($rawPassesTempArray);
        $this->orderedPasses[] = $initialElement;
        $currentStart = $initialElement->getFrom();
        $currentFinish = $initialElement->getDestination();

        if (sizeof($rawPassesTempArray) == 0) {
            return $this->orderedPasses;
        }

        $this->getBorderElement($currentStart, $currentFinish, $rawPassesTempArray);
    }

    /**
     * Recursive method. Finds element (pass) which destination equals current trip start or which start
     * equals current trip finish and append pass to trip
     * @param $currentStart
     * @param $currentFinish
     * @param BoardingPass[] $passes
     */
    private function getBorderElement($currentStart, $currentFinish, $passes)
    {
        foreach ($passes as $index => $pass) {
            if ($pass->getFrom() == $currentFinish) {
                // set current element in the end of the trip
                $currentFinish = $pass->getDestination();
                $this->orderedPasses[] = $pass;
                array_splice($passes, $index, 1);
                break;
            }
            if ($pass->getDestination() == $currentStart) {
                // set current element in the beginning of the trip
                $currentStart = $pass->getFrom();
                array_unshift($this->orderedPasses, $pass);
                array_splice($passes, $index, 1);
                break;
            }
        }
        if (sizeof($passes) > 0) {
            $this->getBorderElement($currentStart, $currentFinish, $passes);
        }
    }

    /**
     * Returns a text description with a list of steps of the journey
     * @return string
     */
    public function getJourneyDescription()
    {
        $journeyDescription = '';
        $stepNumber = 0;
        foreach ($this->orderedPasses as $pass) {
            $stepNumber++;
            $journeyDescription .= $stepNumber . '. ' . $pass->getDescription() . PHP_EOL;
        }

        $stepNumber++;
        $journeyDescription .= $stepNumber . '. You have arrived at your final destination.';

        return $journeyDescription;
    }

    /**
     * Returns an associative array of ordered passes
     * @return array
     */
    protected function getOrderedPassesAsArray()
    {
        $passes = [];
        foreach ($this->getOrderedPasses() as $passObject) {
            $passes[] = [
                'type' => $passObject->getType(),
                'transportNumber' => $passObject->getTransportNumber(),
                'from' => $passObject->getFrom(),
                'destination' => $passObject->getDestination(),
                'seat' => $passObject->getSeat(),
                'gate' => $passObject->getGate(),
                'baggage' => $passObject->getBaggage(),
            ];
        }
        return $passes;
    }


    /**
     * Returns the ordered passes list in the same format as input list
     * @return mixed
     * @todo implement the Strategy pattern for different formats
     */
    public function outputOrderedPasses()
    {
        switch ($this->getFormat()) {
            case self::FORMAT_JSON:
                return json_encode(['items' => $this->getOrderedPassesAsArray()]);
        }
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

}
