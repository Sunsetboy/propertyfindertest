<?php
/**
 * Simple autoloader
 */
spl_autoload_register(function ($classFullName) {
    $classNameParts = explode("\\", $classFullName);
    $class = array_pop($classNameParts);
    include __DIR__ . DIRECTORY_SEPARATOR . $class . '.php';
});
