<?php

namespace TripSorter;

/**
 * Train boarding pass
 * Class TrainPass
 * @package TripSorter
 */
class TrainPass extends BoardingPass
{
    /**
     * @return string
     * return string example: Take train 78A from Madrid to Barcelona. Sit in seat 45B.
     */
    public function getDescription()
    {
        $description = 'Take train '
            . $this->getTransportNumber()
            . ' from '
            . $this->getFrom()
            . ' to ' . $this->getDestination()
            . '. Sit in seat ' . $this->getSeat();

        return $description;
    }
}
