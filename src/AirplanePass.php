<?php

namespace TripSorter;

/**
 * Airplane boarding pass
 * Class TrainPass
 * @package TripSorter
 */
class AirplanePass extends BoardingPass
{
    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        $description = 'From '
            . $this->getFrom()
            . ', take flight '
            . $this->getTransportNumber()
            . ' to ' . $this->getDestination()
            . '. Gate ' . $this->getGate()
            . ', seat ' . $this->getSeat()
            . '. ' . $this->getBaggage();

        return $description;
    }
}
