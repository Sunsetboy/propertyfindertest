# TRIP SORTER
### Test work for PropertyFinder

#### Brief description
My solution can receive unordered list of boarding passes in defferent formats (JSON support realized), sort them and returns ordered list in the same format as input or text description of trip. Now 3 transport types are supported: train, airplane and airport bus. This list could be easy extended by extending the base class BoardingPass.

#### Requirements
PHP installed in your system. Developed on 7.1, but should work with PHP 5.4
#### Usage example
```
// initialize the stack of boarding passes
$stack = new PassesStack($passesJSON, PassesStack::FORMAT_JSON);

// sort it
$stack->sort();

// get a text description of trip
echo $stack->getJourneyDescription();

// get JSON with ordered passes
$orderedListJson = $stack->outputOrderedPasses();
```
where $passesJSON - JSON string. Examples of JSON you can find in *tests/data* folder

#### Testing
Tests are located in *tests* folder. Test data sets are in *tests/data* folder.
To run test, execute it in command line:
```
php tests/MadridToNYCTest.php
php tests/OneStepTripTest.php
```
#### Continious Integration
This step wasn't presented in the task, but I also configured a CI for auto testing on Bitbucket.

#### What could be extended in future
1. Transport types. Now where are 3 of them: train, airplane and airport bus. Developers can add any new type like Intercity bus or Blablacar.
2. Input formats. Now application supports only JSON, but other formats could be added (for example, XML)

I spent about 3.5-4 hours doing this task.

Thank you for interesting task and attention! Feel free to ask me any question about my code.
Michael.